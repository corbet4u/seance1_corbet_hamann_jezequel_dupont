-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mer. 04 avr. 2018 à 09:35
-- Version du serveur :  10.1.26-MariaDB
-- Version de PHP :  7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gamepedia`
--

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `email` varchar(50) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `adresse` varchar(100) NOT NULL,
  `numTel` varchar(8) NOT NULL,
  `dateNaiss` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`email`, `nom`, `prenom`, `adresse`, `numTel`, `dateNaiss`) VALUES
('adelaide.ledoux@yahoo.fr', 'Denis', 'Maggie', '8, avenue Monnier\n34139 Marie', '04 69 13', '2009-03-04'),
('agathe.bertin@free.fr', 'Martinez', 'Jeanne', '81, rue de Tanguy\n99 687 Evrard', '06 64 72', '1998-09-26'),
('ahamel@riou.com', 'Techer', 'Laetitia', '949, rue de Leveque\n17681 Leclercq-sur-Brunet', '07 91 62', '2007-03-27'),
('alice.thierry@paris.com', 'Buisson', 'Julien', 'boulevard de Muller\n10586 Meunier', '+33 (0)8', '1976-09-22'),
('alphonse.michel@live.com', 'Breton', 'Brigitte', 'rue de Pierre\n44391 Bouvetdan', '01 49 64', '1985-10-12'),
('andre.alain@voila.fr', 'Loiseau', 'Dorothée', '686, avenue Diaz\n63 250 FaivreBourg', '08852215', '1998-12-21'),
('arnaude05@lebrun.fr', 'Moreno', 'Émilie', '64, impasse de Aubert\n56380 Renault', '03 33 43', '1975-02-11'),
('astrid.richard@orange.fr', 'Hoarau', 'Christelle', '3, boulevard de Giraud\n10 222 Duhamel', '05658869', '1986-03-17'),
('astrid09@ifrance.com', 'Brun', 'William', '7, impasse de Delmas\n60 901 Ferrand', '+33 (0)1', '1976-08-06'),
('auguste83@dbmail.com', 'Brunet', 'Léon', '28, avenue Blin\n54639 Menard-sur-Hoareau', '+33 4 78', '2011-01-05'),
('augustin.lecomte@dbmail.com', 'Daniel', 'Thibault', '10, impasse Pires\n98 075 Boulanger', '+33 5 14', '2012-10-16'),
('aurore.pascal@sfr.fr', 'Monnier', 'Alexandrie', 'rue Brigitte Paris\n98408 Gerard-sur-Merle', '01660321', '1973-04-26'),
('aurore.robin@yahoo.fr', 'Bernier', 'Claudine', '16, chemin Alexandrie Delmas\n93 901 Peron-sur-Voisin', '02 28 51', '1993-12-05'),
('barthelemy.antoinette@voisin.fr', 'Charles', 'Madeleine', '15, impasse Hoareau\n16 249 Mary', '+33 (0)3', '2009-05-11'),
('bbriand@ifrance.com', 'Leconte', 'Monique', '2, place Henriette Brunel\n85611 Bruneldan', '07814523', '1978-04-22'),
('benjamin.coulon@antoine.com', 'Schmitt', 'Susan', 'rue Jacques Diallo\n53309 Hoareau-sur-Clement', '+33 1 81', '2008-04-14'),
('benjamin.pons@live.com', 'Rousseau', 'Christophe', 'place Inès Ribeiro\n34 418 Marty', '05 85 94', '1983-03-02'),
('benoit.langlois@perrin.com', 'Blanc', 'Éric', 'chemin Joséphine Aubert\n52481 Guerin', '+33 2 08', '1979-07-12'),
('bernadette.lacombe@tele2.fr', 'Joubert', 'Lucie', '831, rue Charrier\n93 081 Legendreboeuf', '04 17 36', '1980-11-14'),
('bernadette.vallet@club-internet.fr', 'Launay', 'Élise', '321, chemin Monique Masse\n92088 Le Roux-sur-Paul', '+33 9 86', '1988-01-10'),
('bhenry@gmail.com', 'Weber', 'Margot', '29, place Adrien Peltier\n66 465 Michaud', '+33 8 03', '1996-06-24'),
('blambert@gmail.com', 'Pineau', 'Thibaut', '852, impasse de Leblanc\n43 199 Ferreiraboeuf', '+33 (0)3', '2000-11-02'),
('bonneau.jacques@live.com', 'Besnard', 'Joséphine', 'rue Guichard\n52212 Boyer-sur-Mer', '07 86 70', '1992-07-30'),
('bonneau.margot@neveu.fr', 'Payet', 'Timothée', '326, impasse Dupuy\n16 613 LeroyBourg', '02730279', '1997-05-11'),
('boulanger.susanne@bouygtel.fr', 'De Sousa', 'Maggie', '388, rue Bertrand Jean\n06427 Gregoire', '+33 (0)4', '1979-05-20'),
('bouvier.philippe@moulin.net', 'Denis', 'Arthur', '45, impasse de Becker\n97541 Levy', '+33 (0)3', '2015-02-20'),
('brigitte.dubois@charpentier.fr', 'Menard', 'Odette', '96, avenue de Arnaud\n16242 Mercier', '07 97 93', '1986-06-11'),
('buisson.elodie@chevallier.net', 'Gosselin', 'Léon', 'place de Lacombe\n15899 Labbe-les-Bains', '06943773', '1972-12-20'),
('camille.rey@orange.fr', 'Goncalves', 'Bernard', '50, boulevard Pereira\n81355 Auger-la-Forêt', '+33 6 98', '1987-01-01'),
('capucine28@tele2.fr', 'Le Goff', 'Robert', '96, chemin de Guillon\n34095 ChauveauBourg', '01410477', '2016-01-24'),
('carre.gabrielle@pruvost.fr', 'Carpentier', 'Yves', '44, boulevard Brun\n73470 Robert-sur-Gautier', '+33 9 75', '1983-07-09'),
('cbrunel@dbmail.com', 'Millet', 'Alexandre', 'rue Lucie Lenoir\n98 290 Moreau-la-Forêt', '01 33 82', '2007-11-20'),
('cecile.marion@wanadoo.fr', 'Le Roux', 'Richard', '219, chemin Launay\n60 701 Marques', '03 52 83', '2014-05-21'),
('chantal.moreau@allard.fr', 'Mahe', 'Jacques', '2, chemin Lesage\n63741 ToussaintBourg', '+33 (0)3', '1983-12-21'),
('charles.gallet@tele2.fr', 'Richard', 'Marthe', '73, chemin Pierre\n46354 Da Costa', '07 35 25', '2004-02-20'),
('charlotte.poirier@pierre.fr', 'Fischer', 'Gabriel', '55, impasse Marc Gregoire\n06 446 BretonBourg', '+33 2 17', '1984-11-12'),
('charlotte.pottier@club-internet.fr', 'Joubert', 'Étienne', 'boulevard David Fontaine\n61986 Ferrand', '+33 8 52', '1975-05-24'),
('chretien.martine@lecoq.com', 'Camus', 'Anouk', '76, place Roland Gimenez\n51254 GayBourg', '+33 (0)5', '1987-04-26'),
('christophe.loiseau@jacquet.fr', 'Perrot', 'Christiane', 'boulevard Alphonse Becker\n61 797 Dumontdan', '06952264', '1982-05-17'),
('claudine.costa@renaud.org', 'Vidal', 'Matthieu', '7, chemin de Aubert\n80 223 Fontaine', '+33 (0)1', '1973-05-15'),
('claudine.fischer@free.fr', 'Lemoine', 'Émile', '70, place Nicolas Berthelot\n64002 BourgeoisBourg', '01061354', '1998-11-18'),
('clemence.lebreton@yahoo.fr', 'Roux', 'Daniel', 'chemin de Langlois\n34576 Buissondan', '+33 4 02', '2011-06-23'),
('cmarchand@live.com', 'Blanchard', 'Théophile', '7, place Devaux\n24 128 Diaz-sur-Seguin', '03157331', '2003-05-30'),
('colette.parent@laposte.net', 'Dijoux', 'Alfred', '71, chemin Thomas Godard\n92906 Foucher', '08523442', '1971-07-14'),
('colette66@ifrance.com', 'Duhamel', 'Alice', '23, impasse de Girard\n49 261 Chauvin', '07955105', '1971-05-24'),
('corinne.fernandez@laposte.net', 'Costa', 'Guillaume', '986, chemin Bouchet\n27815 Allain-sur-Blin', '+33 6 10', '2006-05-26'),
('david76@free.fr', 'Jacquot', 'Yves', '592, avenue de Leblanc\n23 970 Jacob', '+33 4 90', '1971-11-26'),
('delaunay.richard@couturier.org', 'Verdier', 'Diane', '311, boulevard Anne Bouvier\n26 761 Becker', '01921195', '1997-04-22'),
('denis45@club-internet.fr', 'Lenoir', 'Julien', '1, place Fischer\n43080 Caronnec', '+33 7 55', '1992-12-31'),
('devaux.thierry@paul.com', 'Valentin', 'Guillaume', '7, rue de Didier\n35617 Maillot', '+33 (0)3', '2015-10-26'),
('dominique.martins@live.com', 'Bonneau', 'Henri', '52, rue Alves\n38 729 Laroche-sur-Mer', '+33 (0)4', '1982-06-11'),
('dominique.meyer@lebreton.com', 'Marechal', 'Michel', '54, boulevard Labbe\n18721 Marty', '+33 (0)6', '1990-08-08'),
('dominique44@masson.fr', 'Labbe', 'Catherine', '66, rue de Bouchet\n37009 Thierry', '04882403', '1970-03-09'),
('dossantos.adele@clement.fr', 'Delmas', 'Augustin', '39, place de Paris\n69554 Vidal-la-Forêt', '+33 7 42', '2017-11-26'),
('dubois.celina@live.com', 'Morin', 'Noël', '46, place de Tessier\n50 359 Jeannec', '+33 (0)9', '1987-10-12'),
('dumont.joseph@costa.com', 'Sauvage', 'Marie', '8, impasse Rodrigues\n73888 Colinnec', '+33 9 75', '1976-03-06'),
('dupuy.daniel@tiscali.fr', 'Marques', 'Gabrielle', '10, rue Frédéric Arnaud\n50447 Maillet', '05 30 04', '1974-02-20'),
('dupuy.gabriel@diallo.org', 'Guillou', 'Aurore', '3, avenue Brun\n92 424 Peltier', '+33 (0)1', '1991-06-28'),
('ebigot@dumont.com', 'Besnard', 'Clémence', 'place Ferreira\n25 248 Mailletdan', '06 61 52', '2004-05-02'),
('eboucher@devaux.fr', 'Renault', 'Margaret', '46, place Millet\n63 065 Verdier', '03 12 16', '1992-10-23'),
('elecomte@voisin.fr', 'Lombard', 'Théophile', '38, rue Michelle Leleu\n72 393 Blin', '07 38 13', '1994-03-14'),
('eleonore37@noos.fr', 'Peltier', 'Suzanne', '90, impasse Lelievre\n26849 Vallee-sur-De Oliveira', '+33 (0)8', '1973-11-21'),
('elise.roux@faure.fr', 'Masse', 'Colette', '258, rue Marcel Hebert\n71 577 Cohen', '01 20 06', '1993-01-30'),
('emmanuel.simon@gmail.com', 'Chauveau', 'Victor', '12, impasse de Guilbert\n97 676 Hernandez', '01 74 76', '1978-08-29'),
('eugene.neveu@laposte.net', 'Richard', 'Marcel', '64, impasse Fischer\n26601 Ribeiro', '01744749', '1977-04-08'),
('evrard.aime@berthelot.com', 'Pasquier', 'Édouard', '70, chemin de Garcia\n99361 Allard', '+33 9 12', '2013-11-01'),
('fperez@orange.fr', 'Michel', 'Christine', '49, boulevard Audrey Allard\n43 549 Allain', '+33 (0)1', '1977-07-03'),
('frederique.ramos@club-internet.fr', 'Michel', 'Louise', '42, rue Lebrun\n03 127 BouvierVille', '01041315', '2016-02-15'),
('frederique36@live.com', 'Chauvet', 'Henriette', '22, impasse de Devaux\n87338 Bourgeois', '01 91 64', '2002-05-29'),
('gabriel.arnaud@masson.net', 'Auger', 'Alice', '43, chemin Sylvie Schmitt\n00924 OlivierVille', '03500506', '1994-12-20'),
('gabriel.cousin@live.com', 'Fontaine', 'Stéphanie', '6, avenue de Morin\n45 433 Picard', '05 87 67', '1976-01-18'),
('gbuisson@bruneau.org', 'Torres', 'Jacques', '38, place Zoé Lemoine\n43 892 Gillet', '+33 9 44', '2007-12-26'),
('genevieve.paul@hotmail.fr', 'Bernier', 'Théophile', '41, rue Leger\n63 402 Gerard', '01 04 37', '1998-09-12'),
('georges99@bouygtel.fr', 'Colas', 'Suzanne', 'chemin Allard\n86083 Buisson-sur-Courtois', '+33 (0)8', '2006-07-29'),
('gerard.dupuy@sfr.fr', 'Laroche', 'Adrien', '655, avenue Alex Leconte\n33 163 Peron', '+33 (0)4', '2005-11-24'),
('germain.mathilde@leconte.net', 'Roche', 'Hortense', '3, rue Christine Blin\n89 728 Le Gall', '03331634', '1990-05-20'),
('gilles.martin@gmail.com', 'Pierre', 'Denise', '69, chemin Jean\n43 114 Gerard-sur-Dos Santos', '04891233', '1982-12-23'),
('gillet.julien@guillou.com', 'Chretien', 'Patricia', '52, chemin Gilles Chevalier\n32338 EvrardVille', '+33 (0)6', '2013-08-25'),
('glombard@gmail.com', 'Klein', 'Tristan', '23, chemin Clerc\n02 717 Hamon-sur-Ledoux', '+33 (0)5', '2009-01-04'),
('gregoire.besson@guillot.fr', 'Daniel', 'Raymond', 'chemin Bertrand Thierry\n42 309 Riviere', '01756715', '1999-05-01'),
('gregoire.ines@antoine.net', 'Bouchet', 'Pénélope', '67, chemin Colas\n98428 Martins', '+33 (0)3', '2015-08-26'),
('guerin.alphonse@rousseau.fr', 'Herve', 'Jean', 'place Leveque\n31 727 Rocher-la-Forêt', '+33 4 05', '2005-01-03'),
('guibert.mathilde@tiscali.fr', 'Faure', 'Maurice', '677, impasse Rousset\n64 062 Pons', '03 81 01', '1975-02-03'),
('hcharpentier@gmail.com', 'Martineau', 'Alain', '76, rue Véronique Fontaine\n59 194 Dumas', '+33 8 01', '1998-11-29'),
('henri04@germain.com', 'Bertin', 'Joseph', '567, rue de Le Gall\n04465 Rousselboeuf', '+33 9 28', '2008-10-04'),
('hgallet@noos.fr', 'Guillaume', 'Dominique', '92, place Carre\n24189 Bonnin', '01 64 12', '2018-02-26'),
('hleroy@club-internet.fr', 'Henry', 'Nicole', '24, rue Théophile Roy\n98 924 Dumas', '+33 4 40', '2001-06-24'),
('hortense37@lelievre.net', 'Louis', 'Jacques', 'boulevard Martins\n95 445 Hoarau', '04434419', '1971-07-27'),
('hroger@becker.com', 'Letellier', 'Laetitia', '95, boulevard de Gaillard\n69 904 Baron', '08 65 74', '2003-12-09'),
('hugues.guillou@free.fr', 'Raymond', 'Honoré', '4, avenue Albert\n25256 MartinVille', '+33 (0)7', '2012-11-21'),
('ideoliveira@langlois.fr', 'Charles', 'Timothée', '47, chemin Menard\n30 408 Munozboeuf', '03 95 97', '1973-10-14'),
('igregoire@rey.com', 'Charrier', 'Marianne', '272, boulevard de Brun\n59753 Chevalier', '01761798', '1977-07-04'),
('ihamel@gautier.fr', 'Etienne', 'Charles', 'boulevard de Mendes\n52 522 Berthelot-les-Bains', '+33 4 29', '2018-02-02'),
('ines.hamon@chauvin.com', 'Riviere', 'Nicolas', '86, rue Marchand\n62 509 Rolland', '+33 6 24', '1977-06-06'),
('isaac11@guyon.fr', 'Devaux', 'Simone', '6, boulevard Laure Godard\n85 478 Albert-sur-Laporte', '+33 (0)6', '2005-09-17'),
('isauvage@orange.fr', 'Bourdon', 'Nath', '764, avenue Mary\n12897 Diallo', '09 37 25', '1991-12-21'),
('jacob.jacqueline@yahoo.fr', 'Valentin', 'Amélie', '63, rue de Petitjean\n10391 Fournier', '07 39 92', '1990-06-20'),
('jacob.monique@roger.fr', 'Brun', 'Agnès', '34, impasse Céline Collin\n28283 Andre', '01 68 90', '1979-11-29'),
('jeanne92@ifrance.com', 'Renaud', 'Lucas', '3, boulevard Bernard Teixeira\n60 874 Gay', '+33 9 37', '1983-01-08'),
('josette.vasseur@tele2.fr', 'Duval', 'Brigitte', '25, impasse de Reynaud\n20 496 Cordier-sur-Cousin', '06209195', '1998-07-05'),
('klemaitre@free.fr', 'Paris', 'Denis', '400, boulevard Delannoy\n33 795 Andredan', '+33 1 01', '1993-11-12'),
('klopes@deoliveira.org', 'Poirier', 'Jacqueline', '53, impasse Julien\n96684 Le Goff-sur-Leconte', '+33 (0)9', '2012-06-05'),
('knormand@gmail.com', 'Leconte', 'Renée', '47, avenue de Hamel\n66 385 LebretonVille', '02197856', '1973-03-27'),
('lacombe.hugues@sfr.fr', 'Normand', 'Marine', '95, avenue Lebreton\n01 738 Perrierboeuf', '+33 5 10', '1985-12-21'),
('laine.luce@orange.fr', 'Robin', 'Charles', '39, rue Riou\n30507 Dijoux-la-Forêt', '+33 3 04', '1988-04-12'),
('laurent11@letellier.org', 'Neveu', 'Antoine', '7, impasse Théophile Morel\n32 223 Lamy', '+33 1 77', '1976-03-06'),
('lebreton.suzanne@tele2.fr', 'Jean', 'André', '77, chemin de Mace\n47112 Pottiernec', '02 13 02', '2000-02-08'),
('ledoux.alfred@club-internet.fr', 'Voisin', 'Luce', 'rue Olivier\n07826 Verdier-la-Forêt', '+33 (0)7', '1998-08-05'),
('lefevre.alexandria@bouygtel.fr', 'Langlois', 'Xavier', '84, impasse de Leveque\n38746 Toussaint', '+33 (0)5', '1985-09-29'),
('legrand.antoine@tele2.fr', 'Launay', 'Xavier', '1, rue Daniel\n54 739 Bertin', '01247284', '1993-10-29'),
('legrand.marcel@peltier.net', 'Boyer', 'Marie', 'boulevard de Berger\n67 222 Bruneaudan', '08106661', '1998-05-17'),
('llesage@dbmail.com', 'Masson', 'Grégoire', 'place de Vallet\n35537 Guillon', '+33 9 45', '2008-10-15'),
('lmartinez@free.fr', 'Paris', 'Margaux', '783, rue de Julien\n56470 Renard', '+33 3 70', '1970-10-08'),
('lrobert@prevost.com', 'Maillot', 'Chantal', '75, rue de Auger\n48 037 Jacquet', '+33 (0)1', '1992-01-01'),
('lucas16@yahoo.fr', 'Boyer', 'Honoré', '576, impasse Adèle Royer\n40358 Pinto', '+33 9 87', '2007-12-19'),
('luce.ferreira@vallet.com', 'Duval', 'Christelle', '44, chemin de Bourgeois\n43 266 Perret', '09845389', '1978-05-18'),
('lucy.labbe@peltier.org', 'Weiss', 'Luce', '19, avenue de Techer\n37 928 Verdier-sur-Meunier', '+33 1 73', '1972-12-09'),
('mace.arthur@lecomte.com', 'Pereira', 'Marthe', '10, impasse Allard\n19 458 Gomes', '05 38 38', '2006-12-04'),
('mace.christelle@meunier.com', 'Gerard', 'Laure', '58, place de Collin\n80 819 Maillot', '+33 5 35', '2014-09-28'),
('maggie.lelievre@voila.fr', 'Traore', 'Laurence', '13, chemin Agnès Lenoir\n44601 Louis-les-Bains', '09282160', '2013-11-30'),
('maillard.camille@duval.fr', 'Cordier', 'Olivie', '249, place de Perrin\n30122 Gros-sur-Olivier', '+33 3 76', '1983-08-07'),
('manon43@bousquet.fr', 'Delmas', 'Louise', '74, avenue Alexandre Clement\n56 410 Techer', '01 82 01', '1982-09-12'),
('marc.nguyen@gmail.com', 'Charrier', 'Olivie', '94, impasse David Weber\n60 571 GuilbertBourg', '09139495', '1998-10-26'),
('margaret.jacquet@laposte.net', 'Evrard', 'Grégoire', '8, place Virginie Aubry\n58 580 Morel', '05 59 64', '2011-02-15'),
('margaret70@rey.net', 'Barthelemy', 'Édith', 'impasse Marcel Royer\n77 174 Fauredan', '02 43 58', '2014-01-05'),
('margot60@bouchet.com', 'Blin', 'Étienne', '34, avenue Masse\n91 609 Dumas', '+33 6 79', '1973-02-22'),
('marianne.hamel@vidal.com', 'Blanchard', 'Jules', '86, rue Célina Leblanc\n05 279 PrevostVille', '+33 3 17', '1990-04-07'),
('marie.renard@live.com', 'Lemaire', 'Emmanuel', '69, impasse Émile Giraud\n11 936 Guillaume-la-Forêt', '02120786', '2008-01-22'),
('marthe12@jacob.fr', 'Olivier', 'Océane', '180, place de Bailly\n80223 Vidal-sur-Ribeiro', '+33 (0)5', '1989-08-25'),
('marthe50@tiscali.fr', 'Bouvet', 'Roger', '68, impasse de Gosselin\n81842 Voisin', '+33 5 84', '2010-01-12'),
('martine.godard@deoliveira.com', 'Fouquet', 'Hélène', '72, boulevard Denis Martineau\n30 585 Duval', '+33 9 58', '1995-04-21'),
('martine21@leclerc.net', 'Benoit', 'René', '49, place de Martins\n59172 Lejeune', '+33 1 56', '2010-12-18'),
('mary.audrey@potier.com', 'Pereira', 'Sabine', 'chemin Andrée Cohen\n21370 Munoz-la-Forêt', '03854296', '2005-08-16'),
('matthieu.coste@ramos.com', 'Louis', 'Roger', '28, boulevard de Royer\n59672 Imbert-sur-Picard', '+33 (0)5', '1999-09-20'),
('maurice72@laposte.net', 'Berthelot', 'François', '926, chemin Philippine Blin\n31 237 Benoit', '+33 1 27', '1991-05-25'),
('mduhamel@sfr.fr', 'Lecoq', 'Valentine', '987, rue Philippe Leconte\n86302 Simon', '+33 2 25', '1972-06-13'),
('menard.honore@tele2.fr', 'Traore', 'Benoît', '710, rue Didier\n20993 Michaud', '02 04 50', '1975-07-15'),
('meyer.anastasie@boulay.com', 'Delaunay', 'Audrey', '102, impasse Patrick Morel\n48 848 Pruvostnec', '+33 7 31', '1970-02-13'),
('michel.seguin@tele2.fr', 'Daniel', 'Jean', 'impasse Luc Alexandre\n93205 GerardVille', '+33 2 04', '1996-01-06'),
('mmichel@dufour.com', 'Traore', 'Stéphanie', '37, rue Bonnin\n64437 Moulinboeuf', '+33 (0)5', '1993-05-01'),
('moreau.edouard@noos.fr', 'Joseph', 'Marcel', '944, impasse Meunier\n10544 Pages', '06 88 33', '2017-10-13'),
('moreno.thierry@wanadoo.fr', 'Labbe', 'Margaux', '11, avenue Zoé Payet\n47 902 Gaillard-sur-Lebon', '+33 (0)1', '1977-12-08'),
('nathalie.rocher@free.fr', 'Carpentier', 'Suzanne', 'rue Monique Maillot\n30 835 Valletnec', '+33 1 96', '1993-09-10'),
('nathalie.rodrigues@yahoo.fr', 'Roy', 'Capucine', 'place Bousquet\n28 967 Leroynec', '01501051', '1972-11-07'),
('ndelannoy@gimenez.com', 'Ferrand', 'Vincent', '37, boulevard de Denis\n65 977 Brunet', '07969187', '2008-05-25'),
('nicole41@diaz.com', 'Levy', 'Susanne', '514, avenue Lesage\n38 463 Robin', '+33 (0)9', '2004-04-27'),
('nmendes@tiscali.fr', 'Jourdan', 'Marie', '24, rue Valentine Maillot\n40365 Fernandesboeuf', '+33 (0)6', '1997-02-25'),
('noel.chantal@lacombe.net', 'Descamps', 'Joseph', '6, rue David Hardy\n66448 Robert', '01767991', '1995-04-13'),
('noel71@gmail.com', 'Joubert', 'Alfred', '88, place Gabrielle Carlier\n93 989 Nguyen', '+33 9 07', '1976-10-10'),
('obernier@yahoo.fr', 'Carpentier', 'Pénélope', '56, rue Gauthier\n96257 Marty', '01 99 39', '1999-10-04'),
('oblot@orange.fr', 'Chretien', 'Amélie', '9, rue Louis\n56963 Lombard-les-Bains', '+33 7 97', '1999-10-14'),
('olivier.sauvage@breton.com', 'Bernier', 'Emmanuelle', '6, rue de Jean\n10460 Lebrun-la-Forêt', '08004999', '1975-06-06'),
('olivier33@hotmail.fr', 'Imbert', 'Marc', 'boulevard de Faivre\n39 186 Hamon', '04411483', '1994-03-22'),
('paul.paul@cousin.fr', 'Grondin', 'Édouard', 'avenue Stéphanie Ramos\n27 053 Pires-les-Bains', '+33 1 89', '1979-01-22'),
('paul09@joubert.fr', 'Fischer', 'Thierry', '3, chemin Susan Lacroix\n61851 Pereiraboeuf', '02 71 14', '2000-09-25'),
('philippe.lamy@voila.fr', 'Michaud', 'André', '423, chemin de Berthelot\n15 564 Toussaint', '05 42 28', '2010-06-05'),
('pperrin@ifrance.com', 'Fouquet', 'Andrée', '77, boulevard Capucine Denis\n46 807 OlivierVille', '04 86 21', '1970-01-25'),
('qlaunay@orange.fr', 'Charles', 'Noël', '80, chemin Charpentier\n66 180 Rey', '+33 3 54', '1970-11-09'),
('raymond.adrien@gilles.net', 'Jacques', 'Valentine', '76, rue Jérôme Daniel\n87 667 Lelievre', '01 80 05', '1982-08-03'),
('raymond56@hubert.fr', 'Poulain', 'Pauline', 'place Normand\n23 685 Durand-les-Bains', '01 72 44', '2004-06-12'),
('raymond72@sfr.fr', 'Boyer', 'Margot', '755, rue de Michaud\n82854 Costa', '+33 1 21', '2000-01-23'),
('raynaud.richard@live.com', 'Tanguy', 'Bernard', 'impasse de Masse\n18 165 Diallo', '08 00 53', '1989-10-21'),
('rbrunet@leclerc.net', 'Hebert', 'Margot', 'avenue de Peron\n80 474 De Sousa', '+33 (0)3', '2013-01-11'),
('remy82@gmail.com', 'Hoarau', 'Margot', 'avenue de Moreno\n51322 Gonzalez', '01 13 13', '1974-01-28'),
('renaud.veronique@lopes.com', 'Poirier', 'Françoise', '49, boulevard Texier\n02181 Perrin-sur-Arnaud', '05 19 47', '2012-12-20'),
('rene18@jacques.org', 'Fernandez', 'Margot', '774, place de Dumas\n23 385 Boulay', '06626473', '2010-09-03'),
('rene56@foucher.fr', 'Faivre', 'William', '43, rue Raymond\n12337 Germain', '+33 (0)1', '2015-07-01'),
('richard.breton@voila.fr', 'Bourgeois', 'Valentine', 'impasse Robin\n75 623 FernandezVille', '+33 2 47', '2016-06-30'),
('riviere.yves@letellier.com', 'Maillet', 'Honoré', '2, avenue Noémi Caron\n36100 Fernandez', '+33 7 47', '2001-08-14'),
('rmartineau@orange.fr', 'Dubois', 'Grégoire', '59, boulevard de Guerin\n01347 Lemoine', '08 26 82', '2012-08-06'),
('robert34@dacosta.com', 'Lamy', 'Martin', '70, place de Levy\n95113 Raynaud', '+33 8 41', '1983-01-05'),
('rodrigues.alfred@bouygtel.fr', 'Delaunay', 'Margaud', '280, impasse de Toussaint\n22665 Lebon', '02986617', '1998-10-19'),
('rousset.dorothee@tele2.fr', 'Gros', 'Martin', '98, place de Gosselin\n83 298 Gonzalez', '+33 (0)3', '1990-08-27'),
('sguyot@cordier.com', 'Peron', 'Gilbert', '41, rue de Alves\n20 012 Muller-sur-Breton', '+33 8 66', '2001-03-22'),
('simone.mace@colas.fr', 'Daniel', 'Emmanuelle', '35, boulevard de Tessier\n37086 Nguyen', '05 44 55', '1995-07-04'),
('sraynaud@mallet.com', 'Girard', 'Bernard', '26, boulevard Aurélie Dias\n62254 Petitjean', '+33 2 84', '2006-09-28'),
('susanne34@devaux.fr', 'Charrier', 'Nicolas', '45, rue de Lopez\n13 653 LecoqBourg', '04 61 93', '2008-05-15'),
('suzanne.gimenez@tiscali.fr', 'Roussel', 'Nicolas', '25, avenue Roche\n88 032 Leclercq', '09566364', '1998-08-02'),
('theodore20@live.com', 'Perrin', 'Gabrielle', '389, rue de Camus\n24 958 Renaultnec', '04208818', '1971-12-23'),
('theophile71@club-internet.fr', 'Durand', 'Jacques', '82, impasse Roger Foucher\n22663 Marechal-les-Bains', '03671369', '2009-05-05'),
('therese89@orange.fr', 'Baron', 'Alexandria', '132, place Menard\n80769 Salmon-sur-Morvan', '+33 1 94', '1979-01-11'),
('thibaut78@vaillant.fr', 'Pruvost', 'Véronique', '6, avenue Martineau\n76481 Dijoux', '08 38 83', '1985-04-25'),
('timothee10@maillard.com', 'Leblanc', 'Georges', '72, chemin de Fernandes\n90 757 Goncalves', '+33 1 88', '2005-11-19'),
('valette.jacques@ifrance.com', 'Etienne', 'Astrid', '47, boulevard Geneviève Navarro\n20338 Delahaye', '03 55 01', '2015-02-24'),
('vgermain@leclercq.fr', 'Noel', 'Thierry', '537, chemin Deschamps\n00784 Simon-la-Forêt', '03674925', '1980-09-09'),
('vgilles@club-internet.fr', 'Pichon', 'Margaret', '7, rue de Hernandez\n61071 Arnaud-sur-Joubert', '03715056', '1988-02-06'),
('virginie.bouvet@legrand.org', 'Morin', 'Philippe', '61, chemin Paul Joly\n55 409 Pasquierdan', '+33 9 58', '2001-09-13'),
('wgallet@yahoo.fr', 'Humbert', 'Christine', 'rue Raynaud\n14352 Collin', '08 36 37', '2005-10-22'),
('wleconte@texier.com', 'Morvan', 'André', '71, impasse de Begue\n35 944 Moulin-les-Bains', '06818396', '2012-06-05'),
('xguillou@albert.com', 'Roger', 'Jacqueline', '96, chemin Luce Ramos\n49218 Collin', '+33 1 71', '1982-01-26'),
('xlemonnier@millet.fr', 'Guibert', 'Arthur', '1, place Martin Schneider\n51 793 Alexandre', '08 85 53', '2006-10-14'),
('xmeyer@laposte.net', 'Weber', 'Noémi', '59, boulevard Michel Seguin\n57530 RaymondVille', '04439014', '1998-08-28'),
('xpages@bousquet.fr', 'Jourdan', 'Claire', '37, chemin de Legendre\n63165 Fabreboeuf', '09617712', '1977-01-26'),
('xroy@free.fr', 'De Oliveira', 'Éric', '99, chemin de Boucher\n12616 Normand-sur-Mer', '04 34 63', '2013-06-12'),
('yguillou@bouygtel.fr', 'Fontaine', 'Gérard', '8, impasse de Joseph\n79 552 Guyotdan', '02092367', '1996-08-20'),
('yleveque@noos.fr', 'Charpentier', 'Virginie', '4, impasse Aurélie Marchal\n84 622 Labbe-la-Forêt', '06 09 97', '1998-01-08'),
('yves23@lamy.com', 'Dumas', 'Anastasie', '14, boulevard de Benoit\n82564 Vasseur-sur-Mer', '+33 (0)6', '2004-11-28'),
('zacharie.delorme@lenoir.fr', 'Renaud', 'Yves', 'chemin Michel Guichard\n67 851 Dos SantosVille', '+33 (0)1', '1976-10-22'),
('zoe.guichard@sfr.fr', 'Denis', 'Aurélie', '31, rue de Techer\n00399 Jean-sur-Mer', '07 71 27', '1978-04-21'),
('zpages@sfr.fr', 'Le Goff', 'Marianne', '15, boulevard Fouquet\n15 319 Henry-sur-Texier', '+33 (0)1', '1999-10-02');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`email`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
