<?php
require  'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use \appli\Utilisateur as User;
use \appli\Commentaire as Comm;

$db = new DB();
$conf = parse_ini_file('config.ini');
$db->addConnection($conf);
$db->setAsGlobal();
$db->bootEloquent();

$u = new User();
$u->email = "user1@bdd.fr";
$u->nom = "user";
$u->prenom = "user1";
$u->adresse = "iut charlemagne";
$u->numTel = "01234567";
$u->dateNaiss = new DateTime('2011-01-01');
$u->save();

$u1 = new User();
$u1->email = "user2@bdd.fr";
$u1->nom = "user";
$u1->prenom = "user2";
$u1->adresse = "iut charlemagne";
$u1->numTel = "09876543";
$u1->dateNaiss = new DateTime('1666-01-01');
$u1->save();

for ($i=0; $i < 3; $i++) { 
    //$u->comment()->associate();
    //$u1->comment()->associate();
    $cu = new Comm();
    $cu->titre = "Comm".$i;
    $cu->contenu = "Comm de user 1";
    $cu->dateCrea = new DateTime('now');
    $u->comment()->save($cu);
}

for ($i=0; $i < 3; $i++) { 
    //$u->comment()->associate();
    //$u1->comment()->associate();
    $cu = new Comm();
    $cu->titre = "Comm".$i+3;
    $cu->contenu = "Comm de user 2";
    $cu->dateCrea = new DateTime('now');
    $u1->comment()->save($cu);
}