<?php
require  'vendor/autoload.php';
set_time_limit(0);
use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$conf = parse_ini_file('config.ini');
$db->addConnection($conf);
$db->setAsGlobal();
$db->bootEloquent();
DB::connection()->enableQueryLog();

$app = new Slim\Slim();

$app->get('/s4',function (){
    $usr = \appli\Utilisateur::get();
    print(' Les utilisateurs suivants ont posté plus de 5 comms : ');
    foreach ($usr as $u){
        $res = \appli\Commentaire::where('email','like',$u->email)->get();
        $r = 0;
        foreach ($res as $p)
            $r ++;
        if($r > 5)
            print ($u -> nom .', ');
    }

    $mail = 'theodore20@live.com';
    $res = \appli\Commentaire::where('email','like',$mail)->orderBy('dateCrea', 'DESC')->get();
    print('<br>L utilisateur : ' . $mail . ' a poste les commentaires :<br>');
    foreach($res as $t){
        print('  le '. $t->dateCrea .'<br>');
    }
})->name('s4');

$app->run();