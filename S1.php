<?php
require  'vendor/autoload.php';
set_time_limit(0);
use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$conf = parse_ini_file('config.ini');
$db->addConnection($conf);
$db->setAsGlobal();
$db->bootEloquent();
DB::connection()->enableQueryLog();

$app = new Slim\Slim();

$app->get('/Mario',function () {
    $time = microtime(true);
    $res = \appli\Jeux::where('name', 'like', '%Mario%')->get();
    echo("Temps dexecution : ". (microtime(true)-$time) . "<br>");
    foreach ($res as $t){
        print((string)($t->name.'<br>'));
    }
})->name('ListeMario');

$app->get('/Japon',function () {
    $res = \appli\Compagnie::where('location_country', '=', 'Japan')->get();
    foreach ($res as $t){
        print((string)($t->name.'<br>'));
    }
})->name('ListeJapon');

$app->get('/Base',function () {
    $res = \appli\Plateforme::where('install_base', '>=', '10000000')->get();
    foreach ($res as $t){

        print((string)($t->name.'<br>'));
    }
})->name('ListePlateform');

$app->get('/JeuxQ4',function () {
    $res = \appli\Jeux::take(442)->skip(21173)->get();
    foreach ($res as $t){
        print((string)($t->name.'<br>'));
    }
})->name('ListeQ4');

$app->get('/JeuxPage',function () {

    $res = \appli\Jeux::select("name","deck")->take(500)->get();
    foreach ($res as $t){
        echo $t->name;
    }
})->name('JeuxPage');

$app->run();