<?php
require  'vendor/autoload.php';
set_time_limit(0);
use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$conf = parse_ini_file('config.ini');
$db->addConnection($conf);
$db->setAsGlobal();
$db->bootEloquent();
DB::connection()->enableQueryLog();

$app = new Slim\Slim();

$app->get('/Jeux',function () {
    $time = microtime(true);
    $res = \appli\Jeux::select("name")->get();
    echo("Temps dexecution : ". (microtime(true)-$time) . "<br>");
    foreach ($res as $t) {
        echo $t->name . "<br>";
    }
})->name('Jeux');

$app->get('/Log',function () {
    $res = \appli\Jeux::select('name')->where('name', 'like', 'Mario%')->get();
    $q = DB::getQueryLog();
    foreach ($q as $l) {
        var_dump($l);//->toSql()."<br>";
    }
    //var_dump(DB::getQueryLog());
})->name('Log');

$app->get('/CompareTimeJeuNom',function () {
    $time = microtime(true);
    \appli\Jeux::NomPerso("Mario");
    echo("<br>Temps dexecution pour Mario : ". (microtime(true)-$time) . "<br>");

    $time = microtime(true);
    \appli\Jeux::NomPerso("Wolverine");
    echo("<br>Temps dexecution pour Wolverine : ". (microtime(true)-$time) . "<br>");

    $time = microtime(true);
    \appli\Jeux::NomPerso("Wario");
    echo("<br>Temps dexecution pour Wario: ". (microtime(true)-$time) . "<br>");
})->name('CompareTime');


$app->get('/CompareTimeJeuNomVal',function () {
    $time = microtime(true);
    $res = \appli\Jeux::where('name', 'like', '%Mario%')->get();
    echo("Temps dexecution pour Mario : ". (microtime(true)-$time) . "<br>");

    $time = microtime(true);
    $res = \appli\Jeux::where('name', 'like', '%Wolverine%')->get();
    echo("Temps dexecution pour Wolverine : ". (microtime(true)-$time) . "<br>");

    $time = microtime(true);
    $res = \appli\Jeux::where('name', 'like', '%Wario%')->get();
    echo("Temps dexecution pour Wario : ". (microtime(true)-$time) . "<br>");

})->name('CompareTimeJeuNomVal');


$app->get('/LocationCountry',function () {
    $time = microtime(true);
    $res = \appli\Compagnie::CompanyCountry('USA');
    echo("Temps dexecution pour USA : ". (microtime(true)-$time) . "<br>");

    $time = microtime(true);
    $res = \appli\Compagnie::CompanyCountry('UK');
    echo("Temps dexecution pour UK : ". (microtime(true)-$time) . "<br>");

    $time = microtime(true);
    $res = \appli\Compagnie::CompanyCountry('Germany');
    echo("Temps dexecution pour Germany : ". (microtime(true)-$time) . "<br>");
})->name('LocationCountry');

$app->run();