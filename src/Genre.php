<?php
namespace appli;

/**
* 
*/
class Genre extends \Illuminate\Database\Eloquent\Model
{
	protected $table = 'genre';
    	protected $primaryKey = 'id';
    	public $timestamps = false;

    	public function genres()
    	{
    		return $this->belongsToMany('Jeux', 'game2genre',  'genre_id', 'game_id');
    	}
}
