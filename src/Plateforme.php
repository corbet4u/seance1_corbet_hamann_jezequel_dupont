<?php
namespace appli;

/**
* 
*/
class Plateforme extends \Illuminate\Database\Eloquent\Model
{
	protected $table = 'platform';
    	protected $primaryKey = 'id';
    	public $timestamps = false;

    	public function sortie()
    	{
    		return $this->belongsToMany('Jeux', 'game2platform',  'plateform_id', 'game_id');
    	}

    	public function prod()
    	{
    		return $this->belongsTo('Compagnie', 'id');
    	}
}
