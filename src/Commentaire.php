<?php
/**
 * Created by PhpStorm.
 * User: flori
 * Date: 26/03/2018
 * Time: 11:24
 */

namespace appli;


class Commentaire extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'commentaire';
    protected $primaryKey = 'titre';
    public $timestamps = false;

    public static function creationCommentaire($t, $c,$e, $d, $n){
        $coucou = new Commentaire();
        $coucou -> titre = $t;
        $coucou -> contenu = $c;
        $coucou -> dateCrea = $d;
        $coucou -> idJeu = $n;
        $coucou -> email = $e;
        $coucou -> save();
        return $coucou;
    }
}

function user()
{
   return $this->belongsTo('\appli\Utilisateur', 'email');
}