<?php
namespace appli;

/**
* 
*/
class Jeux extends \Illuminate\Database\Eloquent\Model
{
		protected $table = 'game';
    	protected $primaryKey = 'id';
        public $incrementing = false;
    	public $timestamps = false;

    	public function personnage ()
    	{
    		return $this->hasMany('Personnage', 'id');
    	}

    	public function publie ()
    	{
    		return $this->belongsToMany('Compagnie', 'game_publishers', 'game_id', 'comp_id');
    	}

    	public function dev ()
    	{
    		return $this->belongsToMany('Compagnie', 'game_developers', 'game_id', 'comp_id');
    	}

    	public function genres()
    	{
    		return $this->belongsToMany('Genre', 'game2genre', 'game_id', 'genre_id');
    	}

    	public function sortie()
    	{
    		return $this->belongsToMany('Plateforme', 'game2platform', 'game_id', 'plateform_id');
    	}

    	public function rate()
    	{
    		return $this->belongsToMany('\appli\Classement', 'game2rating', 'game_id', 'rating_id');
    	}

    	public function charact()
    	{
    		return $this->belongsToMany('\appli\Personnage', 'game2character', 'game_id', 'character_id');
		}
		
		public static function PersoJeu()
		{
			$res = \appli\Jeux::where('id', '=', 12342)->first();
			$char = $res->charact()->get();
			foreach ($char as $c){
				print($c->name.'	'.$c->deck.'<br>');
			}
		}

		public static function DebuteMario()
		{
			$res = \appli\Jeux::where('name', 'like', 'Mario%')->get();
			foreach ($res as $jeu){
				echo '<b>'.$jeu->name.'</b>';
				$char=$jeu->charact()->get();
				foreach ($char as $c) {
					print('<p><b>name :</b> '.$c->name.', <b>description : </b></p>'.$c->deck.'<br><br>');
				}
			}
		}

        public static function NomPerso($tmp)
        {
            $res = \appli\Jeux::where('name', 'like', $tmp.'%')->get();
            foreach ($res as $jeu){
                echo '<b>'.$jeu->name.'</b>';
                $char=$jeu->charact()->get();
            }
        }

		public static function RateMario()
		{
			$res = \appli\Jeux::where('name', 'like', 'Mario%')->get();
			foreach ($res as $jeu) {
				echo '<b>'.$jeu->name.'</b><br>';
				$rate=$jeu->rate()->get();
				foreach ($rate as $r) {
					echo $r->name;
				}
				echo '<br><br>';
			}
		}

		public static function Q6() {
			$res = \appli\Jeux::where('name', 'like', 'Mario%');
			$r = $res->rate()->where('name', 'like', '3+')->get();
		}
}
