<?php
namespace appli;

/**
* 
*/
class Classement extends \Illuminate\Database\Eloquent\Model
{
	protected $table = 'game_rating';
    	protected $primaryKey = 'id';
    	public $timestamps = false;

    	public function rate()
    	{
    		return $this->belongsToMany('Jeux', 'game2rating', 'rating_id', 'game_id');
    	}
}
