<?php
/**
 * Created by PhpStorm.
 * User: flori
 * Date: 26/03/2018
 * Time: 11:22
 */

namespace appli;


class Utilisateur extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'utilisateur';
    protected $primaryKey = 'email';
    public $timestamps = false;
    public $incrementing = false;

    public static function creationUtilisateur($n,$p,$e,$a,$d,$t){
        $coucou = new Utilisateur();
        $coucou -> nom = $n;
        $coucou -> prenom = $p;
        $coucou -> adresse = $a;
        $coucou -> numTel = $t;
        $coucou -> dateNaiss = $d;
        $coucou -> email = $e;
        $coucou -> save();

        return $coucou;
    }

function comment()
{
   return $this->hasMany('\appli\Commentaire', 'email');
}
}