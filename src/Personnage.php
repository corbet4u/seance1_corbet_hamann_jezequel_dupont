<?php
namespace appli;

/**
* 
*/
class Personnage extends \Illuminate\Database\Eloquent\Model
{
	protected $table = 'character';
    	protected $primaryKey = 'id';
    	public $timestamps = false;

    	public function charact()
    	{
    		return $this->belongsToMany('Jeux', 'game2character', 'character_id', 'game_id');
    	}

    	public function friends()
    	{
    		return $this->belongsToMany('Personnage', 'friends', 'char1_id', 'char2_id');
    	}

    	public function ennemies()
    	{
    		return $this->belongsToMany('Personnage', 'enemies', 'char1_id', 'char2_id');
    	}
}
