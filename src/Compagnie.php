<?php
namespace appli;

/**
* 
*/
class Compagnie extends \Illuminate\Database\Eloquent\Model
{
	protected $table = 'company';
    	protected $primaryKey = 'id';
    	public $timestamps = false;

    	public function publie ()
    	{
    		return $this->belongsToMany('\appli\Jeux', 'game_publishers', 'comp_id', 'game_id');
    	}

    	public function dev ()
    	{
    		return $this->belongsToMany('\appli\Jeux', 'game_developers', 'comp_id', 'game_id');
    	}

    	public function prod()
    	{
    		return $this->hasMany('\appli\Plateforme', 'id');
		}
		
		public static function DevSony()
		{
			$res = \appli\Compagnie::where('name', 'like', '%Sony%')->get();
			foreach ($res as $comp){
				echo '<b>'.$comp->name.'</b>';
				$jeu=$comp->dev()->get();
				foreach ($jeu as $j) {
					echo '<p><b>name :</b> '.$j->name.', <b>description : </b></p>'.$j->deck.'<br><br>';
				}
			}
		}

        public static function CompanyCountry($pays)
        {
            $res = \appli\Compagnie::where('location_country', 'like', $pays)->get();
        }

}
