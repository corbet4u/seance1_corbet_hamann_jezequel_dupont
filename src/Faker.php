<?php
    namespace appli;
    require  'vendor/autoload.php';

class Faker
{
    public static function creation()
    {
        $faker = \Faker\Factory::create('fr_FR');
        for ($x = 0; $x < 200; $x++) {
            $nom = $faker->lastName;
            $prenom = $faker->firstName;
            $email = $faker->email;
            $numTel = $faker->phoneNumber;
            $adresse = $faker->address;
            $dateNai = $faker->date();

           \appli\Utilisateur::creationUtilisateur($nom, $prenom, $email, $adresse, $dateNai, $numTel);

            $res = \appli\Utilisateur::where('email','like',$email)->first();
            $tmp = $res->email;

            for ($y = 0; $y < 10; $y++) {
                $titre = $faker->sentence(10);
                $contenu = $faker->realText();
                $dateC = $faker->date();
                $nb = rand(1,45000);
                \appli\Commentaire::creationCommentaire($titre, $contenu, $tmp, $dateC, $nb);
            }
        }
    }
}