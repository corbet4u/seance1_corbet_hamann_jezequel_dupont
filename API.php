<?php
require  '../vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$conf = parse_ini_file('../config.ini');
$db->addConnection($conf);
$db->setAsGlobal();
$db->bootEloquent();
DB::connection()->enableQueryLog();

$app = new Slim\Slim();

$app->get('/api/games/:id',function ($id) {
    $res = \appli\Jeux::where('id', '=', $id)->first();
    header('Content-Type: application/json');
    return htmlentities($res->toJson());
})->name('JeuxID');

$app->get('/api/games',function () {
    $res = \appli\Jeux::take(200)->get();
    header('Content-Type: application/json');
    /*
    $games = new \appli\Games();
    $games->games = array();
    $games->games = $res->toJson();
    $games->links->prev->href="";
    $games->links->next->href="";
    echo htmlentities(json_encode($games));*/
    return htmlentities($res->toJson());
})->name('Jeux');

$app->run();