<?php
require  'vendor/autoload.php';
set_time_limit(0);
use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$conf = parse_ini_file('config.ini');
$db->addConnection($conf);
$db->setAsGlobal();
$db->bootEloquent();
DB::connection()->enableQueryLog();

$app = new Slim\Slim();

$app->get('/12342',function () {
    \appli\Jeux::PersoJeu();
})->name('');

$app->get('/StartMario',function () {
    $time = microtime(true);
    \appli\Jeux::DebuteMario();
    echo("<br>Temps dexecution : ". (microtime(true)-$time) . "<br>");
})->name('');

$app->get('/SonyDev',function () {
    \appli\Compagnie::DevSony();
})->name('');

$app->get('/Mario3p',function () {
    $res = \appli\Jeux::where('name', 'like', 'Mario%')->get();
    foreach ($res as $t){
        $temp = $t->rate()->get();
        //if($temp->name =='PEGI: 3+'){
            //print((string)($t->name.'<br>'));
            print((string)($temp->name.'<br>'));
        //}
    }
})->name('Mario3p');

$app->get('/Mario3',function () {
    $res = \appli\Jeux::where('name', 'like', 'Mario%')->get();
    foreach ($res as $t){
        $temp = $t->charact()->count();
        if($temp >=3){
            print((string)($t->name.'<br>'));
        }
    }
})->name('Mario3');

$app->get('/RateMario', function () {
    \appli\Jeux::RateMario();
});

$app->run();